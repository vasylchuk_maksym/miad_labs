# import numpy as np
# import pandas as pd
# # Read in the csv file
# votes = pd.read_csv("114_congress.csv")



# As you can see, there are 100 senators, and they voted on 15 bills (we subtract 3 because the first 3 columns aren't bills).
#print(votes.shape)
#
# # We have more "Yes" votes than "No" votes overall
# print(pd.value_counts(votes.iloc[:,3:].values.ravel()))
#
# import pandas as pd
#
# # The kmeans algorithm is implemented in the scikits-learn library
# from sklearn.cluster import KMeans
#
# # Create a kmeans model on our data, using 2 clusters. random_state helps ensure that the algorithm returns the same results each time.
# kmeans_model = KMeans(n_clusters=4, random_state=1).fit(votes.iloc[:, 3:])
#
# # These are our fitted labels for clusters -- the first cluster has label 0, and the second has label 1.
# labels = kmeans_model.labels_
#
# # The clustering looks pretty good!
# # It's separated everyone into parties just based on voting history
# print(pd.crosstab(labels, votes["party"]))


import numpy as np
import cv2
import matplotlib.pyplot as plt

original_image = cv2.imread("image1.jpeg")
img=cv2.cvtColor(original_image,cv2.COLOR_BGR2RGB)
vectorized = img.reshape((-1,3))
vectorized = np.float32(vectorized)
criteria = (cv2.TERM_CRITERIA_EPS + cv2.TERM_CRITERIA_MAX_ITER, 10, 1.0)
K = 3
attempts=10
ret,label,center=cv2.kmeans(vectorized,K,None,criteria,attempts,cv2.KMEANS_PP_CENTERS)
center = np.uint8(center)
res = center[label.flatten()]
#result_image є результатом кадру, який піддався к-середньому кластеризації.
result_image = res.reshape((img.shape))
figure_size = 15
plt.figure(figsize=(figure_size,figure_size))
plt.subplot(1,2,1),plt.imshow(img)
plt.title('Original Image'), plt.xticks([]), plt.yticks([])
plt.subplot(1,2,2),plt.imshow(result_image)
plt.title('Segmented Image when K = %i' % K), plt.xticks([]), plt.yticks([])
plt.show()