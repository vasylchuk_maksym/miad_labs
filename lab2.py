import numpy as np
import pandas as pd

df = pd.read_csv('mlbootcamp5_train.csv', sep=';',index_col='Confirmed')

data = df.groupby(['gender'])['height'].mean()
print('#1')
print("{} мужчин и {} женщин.".format(sum(df['gender'] == 2),
                                            sum(df['gender'] == 1)))

man_alco = df[df['gender'] == 2]['alco']

woomen_alco = df[df['gender'] == 1]['alco']
print('#2')
print("Мужчини чаще указывают что употребляют алкоголь - {}, чем женщины - {}".format(sum(man_alco == 1), sum(woomen_alco == 1)))

man_smoke = df[df['gender'] == 2]['smoke']
woomen_smoke = df[df['gender'] == 1]['smoke']
print('#3')
print("{}% курящих мужчин и {}% курящих женщин".format(round(100 * man_smoke.mean(), 1), round(100 * woomen_smoke.mean(), 1)))
print("В {}, процент курящих мужчин больше чем женщин".format(round(round(100 * man_smoke.mean(), 1)/round(100 * woomen_smoke.mean(), 1),1)))


smoking = df[df['smoke'] == 1]['age']
nosmoking = df[df['smoke'] == 0]['age']
print('#4')
print("На {} месяцев".format((nosmoking.median() - smoking.median())/30))

age_years = round(df['age'] / 365, 0)
df.insert(loc=len(df.columns), column='age_years', value=age_years)


old_man = df[(df['gender'] == 2) & (df['age_years'] > 60) & (df['age_years'] <= 64)]
old_man_smoke = old_man[old_man['smoke'] == 1]

firstGroup = old_man_smoke[(old_man_smoke['ap_hi'] < 120) & (old_man_smoke['cholesterol'] == 1)]
secondGroup = old_man_smoke[(old_man_smoke['ap_hi'] >= 160) & (old_man_smoke['ap_hi'] < 180)  & (old_man_smoke['cholesterol'] == 3)]


print('#5')
print("В {}, раз в первой группе больше чем во второй".format(sum(firstGroup['cardio'] == 1)/sum(secondGroup['cardio'] == 1)))


bmi = ( df['weight'] / (( df['height'] / 100) ** 2) )
df.insert(loc=len(df.columns), column='bmi', value=bmi)
bmi_median = df['bmi'].median()
bmi_median_wooman = df[df['gender'] == 1]['bmi'].median()
bmi_median_man = df[df['gender'] == 2]['bmi'].median()
bmi_median_health = df[df['cardio'] == 0]['bmi'].median()
bmi_median_healthnt = df[df['cardio'] == 1]['bmi'].median()
bmi_median_health_alco = df[(df['alco'] == 0) & (['cardio'] == 0)]['bmi'].median()
bmi_median_health_alco_man = df[(df['alco'] == 0) & (df['cardio'] == 0) & (df['gender'] == 2)]['bmi'].median()
bmi_median_health_alco_wooman = df[(df['alco'] == 0) & (df['cardio'] == 0) & (df['gender'] == 1)]['bmi'].median()

print('#6')
print("{} - медіанний ВМІ перевищує норму".format(bmi_median) )
print("{} - медіанний ВМІ жінок".format(bmi_median_wooman))
print("{} - медіанний ВМІ чоловіків".format(bmi_median_man))
print("{} - медіанний ВМІ здорових".format(bmi_median_health))
print("{} - медіанний ВМІ хворих".format(bmi_median_healthnt))
print("{} - медіанний ВМІ здорових і вживаючих алкоголь чоловіків".format(bmi_median_health_alco_man))
print("{} - медіанний ВМІ здорових і вживаючих алкоголь жінок".format(bmi_median_health_alco_wooman))

k = df[df['ap_hi']<df['ap_lo']]

w = df[(df['height'] > df['height'].quantile(.975)) | (df['height'] < df['height'].quantile(.025))]

z = df[(w['weight'] > df['weight'].quantile(.975)) | (df['weight'] < df['weight'].quantile(.025))]


perc = (df['gender'].count() / 100)

e = k['gender'].count() + w['gender'].count() + z['gender'].count();
print('#7')
print("Столько процентов данных мы выкинули - ", round(e/perc, 1),"%")
